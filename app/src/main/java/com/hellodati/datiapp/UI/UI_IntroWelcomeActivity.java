package com.hellodati.datiapp.UI;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hellodati.datiapp.Adapter.LangAdapter;
import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;
import com.hellodati.datiapp.SSAPI.Model.Hotel;
import com.hellodati.datiapp.SSAPI.Model.Tourist;
import com.ncorti.slidetoact.SlideToActView;

public class UI_IntroWelcomeActivity extends AppCompatActivity {

    Activity activity;
    public static ImageView hotelImg,hotelLogo;
    public static TextView hotelTitle;
    public static TextView touristName;
    public static TextView spinner;
    public static RatingBar hotelStars;
    public static SlideToActView btnSuivant;
    public static Hotel hotel;
    public static Tourist tourist;
    public static State stateDAO=new State();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=this;
        //init layout
        initContent();
        spinner.setText(LocaleHelper.getLanguage(this));
        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopupLangsSelect(activity);
            }
        });
        btnSuivant.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideToActView slideToActView) {
                Intent intent = new Intent(activity, UI_IntroActivity.class);
                activity.startActivity(intent);
            }
        });

        stateDAO.getAll("fr","iIntroWelcome.getData","iIntroWelcome.getDataError");
        /*doTask send=new doTask();
        send.execute();*/
    }

    public void initContent(){
        setContentView(R.layout.activity_ui__intro_welcome);
        hotelImg=findViewById(R.id.hotel_image);
        hotelLogo=findViewById(R.id.hotel_logo);
        hotelTitle=findViewById(R.id.hotel_title);
        touristName=findViewById(R.id.tourist_name);
        hotelStars=findViewById(R.id.hotel_stars);
        btnSuivant=findViewById(R.id.btn_suivant);
        spinner=findViewById(R.id.spinner);
    }

    /*public static class doTask extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
        }


        protected String doInBackground(Void... urls) {
            return null;
        }


        protected void onPostExecute(String response) {
            setData();
        }
    }

    public static void setData(){

    }*/


    public void ShowPopupLangsSelect(final Activity activity) {
        final Dialog myDialogLangs = new Dialog(activity);
        myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final LangAdapter adapter = new LangAdapter(activity);
        adapter.setSelected(LocaleHelper.getLanguage(activity));
        View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_change_lang, null, false);
        myDialogLangs.setContentView(listItem);
        listItem.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialogLangs.dismiss();
            }
        });
        ListView langsListView = listItem.findViewById(R.id.langs_list);
        langsListView.setAdapter(adapter);
        langsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                LangAdapter.Lang langSelected = adapter.getItem(position);
                if (langSelected != null && !langSelected.getIso().equals(LocaleHelper.getLanguage(activity))) {
                    myDialogLangs.dismiss();
                    String selectedLang = adapter.getItem(position).getIso();
                    LocaleHelper.setLocale(activity, selectedLang);
                    activity.recreate();
                }
            }
        });
        myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        myDialogLangs.show();
    }

    public static Intent RegularLaunch(Context context) {
        Intent intent = new Intent(context, UI_IntroWelcomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
