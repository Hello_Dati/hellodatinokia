package com.hellodati.datiapp.UI;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hellodati.datiapp.Adapter.DrawerListAdapter;
import com.hellodati.datiapp.Adapter.LangAdapter;
import com.hellodati.datiapp.R;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;

import cc.mvdan.accesspoint.WifiApControl;

import static android.content.Context.WIFI_SERVICE;

public class AccountFragment implements ExpandableListView.OnChildClickListener, NavigationView.OnNavigationItemSelectedListener {
    private static Activity activity;
    private DrawerLayout drawerLayout;
    public static String tourist_name;
    public NavigationView navigationView;
    private WifiManager.LocalOnlyHotspotReservation mReservation;


    private ExpandableListView listView;

    public AccountFragment(final Activity activity, final DrawerLayout drawerLayout) {
        this.activity = activity;
        this.drawerLayout = drawerLayout;
        listView = drawerLayout.findViewById(R.id.lvExp);
        initCustomNavList();
        listView.setOnChildClickListener(this);
        navigationView = activity.findViewById(R.id.account_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                switch (groupPosition) {
                    //commands
                    case 0:
                        Intent intent0 = new Intent(activity.getApplicationContext(), HCommands.class);
                        activity.startActivity(intent0);
                        break;
                        //reservations
                    case 1:
                        Intent intent1 = new Intent(activity.getApplicationContext(), HReservations.class);
                        activity.startActivity(intent1);
                        break;
                    //about
                    case 4:
                        Intent intent4 = new Intent(activity.getApplicationContext(), About.class);
                        activity.startActivity(intent4);
                        break;
                }
                return false;
            }
        });
        MainActivity.badge_nav_notif = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.account_nav_notif));
        MainActivity.switch_hotspot = MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_hotspot_switch)).findViewById(R.id.switcher);


        MainActivity.switch_hotspot.setChecked(false);
        MainActivity.switch_hotspot.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                //Snackbar.make(v, (MainActivity.switch_hotspot.isChecked()) ? "is checked!!!" : "not checked!!!", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                if (MainActivity.switch_hotspot.isChecked()) {
               /*     turnOnHotspot();
                    HotspotChannelWrite();*/
                    showPopupHotspot();
                } else {
                    turnOffHotspot();
                }
            }
        });
    }

    public void HotspotChannelWrite() {
        WifiManager wifiManager = (WifiManager) activity.getSystemService(WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = "TipturInfo";
        netConfig.preSharedKey = "hellodati";
        netConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.NONE);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        try {

            Method setWifiApMethod = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            boolean apstatus = (Boolean) setWifiApMethod.invoke(wifiManager, netConfig, true);

            Method isWifiApEnabledmethod = wifiManager.getClass().getMethod("isWifiApEnabled");
            while (!(Boolean) isWifiApEnabledmethod.invoke(wifiManager)) {
            }

            Method getWifiApStateMethod = wifiManager.getClass().getMethod("getWifiApState");
            int apstate = (Integer) getWifiApStateMethod.invoke(wifiManager);

            Method getWifiApConfigurationMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
            netConfig = (WifiConfiguration) getWifiApConfigurationMethod.invoke(wifiManager);

            // For Channel change
            Field wcAdhocFreq = WifiConfiguration.class.getField("frequency");
            int freq = 2462; // default to channel 11
            wcAdhocFreq.setInt(netConfig, freq);
            // For Saving Data
            wifiManager.saveConfiguration();

        } catch (IllegalFormatException ife) {
            ife.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        wifiManager.saveConfiguration();
        WifiApControl apControl = WifiApControl.getInstance(activity.getApplicationContext());

        apControl.setEnabled(netConfig, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void turnOnHotspot() {
        WifiManager manager = (WifiManager) activity.getApplicationContext().getSystemService(WIFI_SERVICE);

        manager.startLocalOnlyHotspot(new WifiManager.LocalOnlyHotspotCallback() {

            @Override
            public void onStarted(WifiManager.LocalOnlyHotspotReservation reservation) {
                super.onStarted(reservation);
                mReservation = reservation;
            }

            @Override
            public void onStopped() {
                super.onStopped();
                WifiManager wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(false);
            }

            @Override
            public void onFailed(int reason) {
                super.onFailed(reason);
            }
        }, new Handler());
    }

    private void turnOffHotspot() {
        if (mReservation != null) {
            mReservation.close();
        }
    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        switch (groupPosition) {
            //dashboard
            case 2:
                if (childPosition == 0) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    activity.startActivity(intent);
                }
                else if (childPosition == 1) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(intent);
                }
                break;
            //configuration
            case 3:
                ShowPopupLangsSelect();
                break;
        }
        return true;
    }

    private void initCustomNavList() {
        List<String> listDataHeader = new ArrayList<>();
        HashMap<String, List<String>> listHash = new HashMap<>();
        listDataHeader.add(activity.getResources().getString(R.string.drawer_item_commands));
        listDataHeader.add(activity.getResources().getString(R.string.drawer_item_reservations));
        listDataHeader.add(activity.getResources().getString(R.string.drawer_item_dashboard));
        listDataHeader.add(activity.getResources().getString(R.string.drawer_item_configuration));
        listDataHeader.add(activity.getResources().getString(R.string.drawer_item_about));

        List<String> commands = new ArrayList<>();
        List<String> reservations = new ArrayList<>();

        List<String> tab_bord = new ArrayList<>();
        tab_bord.add(activity.getResources().getString(R.string.drawer_item_wifi));
        tab_bord.add(activity.getResources().getString(R.string.drawer_item_gps));

        List<String> config = new ArrayList<>();
        //config.add(activity.getResources().getString(R.string.drawer_item_update));
        config.add(activity.getResources().getString(R.string.drawer_item_language));

        List<String> profile = new ArrayList<>();
        profile.add(activity.getResources().getString(R.string.drawer_item_edit_profile));
        profile.add(activity.getResources().getString(R.string.drawer_item_change_password));

        List<String> about = new ArrayList<>();

        listHash.put(listDataHeader.get(0), commands);
        listHash.put(listDataHeader.get(1), reservations);
        listHash.put(listDataHeader.get(2), tab_bord);
        listHash.put(listDataHeader.get(3), config);
        listHash.put(listDataHeader.get(4), about);

        ExpandableListAdapter listAdapter = new DrawerListAdapter(activity, listDataHeader, listHash);
        listView.setAdapter(listAdapter);
    }


    public void ShowPopupLangsSelect() {
        final Dialog myDialogLangs = new Dialog(activity);
        myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final LangAdapter adapter = new LangAdapter(activity);
        adapter.setSelected(LocaleHelper.getLanguage(activity));
        View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_change_lang, null, false);
        myDialogLangs.setContentView(listItem);
        listItem.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialogLangs.dismiss();
            }
        });
        ListView langsListView = listItem.findViewById(R.id.langs_list);
        langsListView.setAdapter(adapter);
        langsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                LangAdapter.Lang langSelected = adapter.getItem(position);
                if (langSelected != null && !langSelected.getIso().equals(LocaleHelper.getLanguage(activity))) {
                    myDialogLangs.dismiss();
                    String selectedLang = adapter.getItem(position).getIso();
                    LocaleHelper.setLocale(activity, selectedLang);
                    activity.recreate();
                }
            }
        });
        myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        myDialogLangs.show();
    }

    public static void initializeCountDrawer() {
        //Gravity property aligns the text
        MainActivity.badge_nav_notif.setGravity(Gravity.CENTER_VERTICAL);
        MainActivity.badge_nav_notif.setTypeface(null, Typeface.BOLD);
        MainActivity.badge_nav_notif.setTextColor(activity.getResources().getColor(R.color.colorAccent));
        MainActivity.badge_nav_notif.setText("99+");

/*

        if (HelloDatiApplication.get() != null) {
            HelloDatiApplication.get().getHelloDatiService().fetchDevice()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new MyDisposableObserver<DevicesResponse>() {
                        @Override
                        public void onNext(DevicesResponse devicesResponse) {
                            if (devicesResponse.getData().size() > 0) {
                                int nbr;
                                nbr = devicesResponse.getData().get(0).getUnread_notifications();
                                if (nbr == 0) {
                                    MainActivity.badge_nav_notif.setVisibility(View.GONE);
                                } else if (nbr > 99) {
                                    MainActivity.badge_nav_notif.setVisibility(View.VISIBLE);
                                    MainActivity.badge_nav_notif.setText("99+");
                                } else {
                                    MainActivity.badge_nav_notif.setVisibility(View.VISIBLE);
                                    MainActivity.badge_nav_notif.setText(nbr + "");
                                }
                            }
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onUnhandledError(Throwable e) {

                        }


                    });
        }

        if (HelloDatiApplication.get() != null) {
            HelloDatiApplication.get().getHelloDatiService().fetchNotification()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new MyDisposableObserver<NotifDataResponse>() {
                        @Override
                        public void onNext(NotifDataResponse notifDataResponse) {
                            if (notifDataResponse.getData().size() > 0) {
                                int nbr = 0;
                                for (int i = 0; i < notifDataResponse.getData().size(); i++) {
                                    if (notifDataResponse.getData().get(i).getSeen() == 0) {
                                        nbr++;
                                    }
                                }
                                if (nbr == 0) {
                                    MainActivity.badge_nav_notif.setVisibility(View.GONE);
                                } else if (nbr > 99) {
                                    MainActivity.badge_nav_notif.setVisibility(View.VISIBLE);
                                    MainActivity.badge_nav_notif.setText("99+");
                                } else {
                                    MainActivity.badge_nav_notif.setVisibility(View.VISIBLE);
                                    MainActivity.badge_nav_notif.setText(nbr + "");
                                }
                            }
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onUnhandledError(Throwable e) {

                        }


                    });
        }*/

    }

    public static void ClearData() {
        final Dialog myDialogLangs = new Dialog(activity);
        myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_clear_data, null, false);
        myDialogLangs.setContentView(listItem);
        listItem.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialogLangs.dismiss();
            }
        });
        listItem.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.successfully_clear_data_text), Toast.LENGTH_LONG).show();
                myDialogLangs.dismiss();
                Intent intent = new Intent(activity, UI_IntroWelcomeActivity.class);
                activity.startActivity(intent);
            }
        });
        myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        myDialogLangs.show();
    }

    public static void clearWithoutAsking() {
        List<String> list = new ArrayList<>();
        list.add("com.Ilock.Ios10.Iphonelockscreen");
        list.add("com.android.providers.telephony");
        list.add("com.mtk.telephony");
        list.add("com.android.contacts");
        list.add("com.android.settings");
        list.add("com.mediatek.camera");
        list.add("com.mediatek.emcamera");
        list.add("com.google.android.apps.photos");
        list.add("com.google.android.calculator");
        list.add("com.zacharee1.systemuituner");
        list.add("com.google.android.ext.services");
        list.add("com.android.providers.telephony");
        list.add("com.google.android.ext.services");
        list.add("com.mediatek.location.lppe.main");
        list.add("com.android.exlist.add(ternalstorage");
        list.add("com.mediatek.ygps");
        list.add("com.mediatek.simprocessor");
        list.add("com.android.companiondevicemanager");
        list.add("com.android.mms.service");
        list.add("com.android.providers.downloads");
        list.add("com.mediatek.batterywarning");
        list.add("com.google.android.music");
        list.add("com.google.android.apps.maps");
        list.add("com.google.android.calculator");
        list.add("com.android.dialer");
        list.add("com.google.android.videos");
        list.add("com.android.phone");
        list.add("com.android.companiondevicemanager");
        list.add("com.android.mms.service");
                    /*for(int i=0;i<list.size();i++){
                        clearPreferences();
                    }*/

        //contacts
        deleteAllContacts();
        //phone calls
        deleteAllPhoneCalls();
        //sms                     *************-----------
        deleteAllSms();
        //account
        //deleteAllAccounts();   *************************
        //images/videos
        deleteAllPhotos_Videos();
        //files
        String root = Environment.getDataDirectory().toString();
        File file = new File(root);
        file.delete();
        if (file.exists()) {
            try {
                file.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file.exists()) {
                activity.getApplicationContext().deleteFile(file.getName());
            }
        }


                    /*Intent intent=new Intent(activity, IntroActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);*/
    }

    //delete functions
    private static void deleteAllPhotos_Videos() {
        /*ArrayList<String> galleryImages = getAllShownImagesPath();
        ArrayList<String> galleryVideos = getAllShownVideosPath();
        for(int i=0;i<galleryImages.size();i++){
            File file = new File(galleryImages.get(i));
            if(file.exists())
            {
                file.delete();
            }
        }
        for(int i=0;i<galleryVideos.size();i++){
            File file = new File(galleryVideos.get(i));
            getDriveResourceClient()
                    .delete(file)
                    .addOnSuccessListener(this,
                            aVoid -> {
                                showMessage(getString(R.string.file_deleted));
                                finish();
                            })
                    .addOnFailureListener(this, e -> {
                        Log.e(TAG, "Unable to delete file", e);
                        showMessage(getString(R.string.delete_failed));
                        finish();
                    });
            if(file.exists())
            {
                file.delete();
            }
        }*/
        try {
            /*Process proc = */
            Runtime.getRuntime().exec("rm -rf /storage/emulated/0/DCIM/Camera/*");
            /*Process proc = */
            Runtime.getRuntime().exec("rm -rf /storage/emulated/0/Pictures/Screenshots/*");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<String> getAllShownImagesPath() {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = activity.getApplicationContext().getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    private static ArrayList<String> getAllShownVideosPath() {
        Uri uri;
        Cursor cursor;
        int column_index_data;
        ArrayList<String> listOfAllVideos = new ArrayList<>();
        String absolutePathOfVideo = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Video.Media.BUCKET_DISPLAY_NAME, MediaStore.Video.Media._ID, MediaStore.Video.Thumbnails.DATA};

        cursor = activity.getApplicationContext().getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfVideo = cursor.getString(column_index_data);

            listOfAllVideos.add(absolutePathOfVideo);
        }
        return listOfAllVideos;
    }

    private static void deleteAllAccounts() {
        Account[] accounts = AccountManager.get(activity.getApplicationContext()).getAccounts();
        for (int i = 0; i < accounts.length; i++) {
            AccountManager.get(activity.getApplicationContext()).removeAccountExplicitly(accounts[i]);
        }
    }

    private static void deleteAllSms() {
        /*Uri deleteUri = Uri.parse(String.valueOf(Telephony.Sms.CONTENT_URI));
        int count = 0;
        Cursor c = activity.getContentResolver().query(deleteUri, new String[]{BaseColumns._ID}, null,
                null, null); // only query _ID and not everything
        try {
            while (c.moveToNext()) {
                // Delete the SMS
                String pid = c.getString(Integer.parseInt(Telephony.Sms._ID)); // Get _id;
                String uri = String.valueOf(Telephony.Sms.CONTENT_URI.buildUpon().appendPath(pid));
                count = activity.getContentResolver().delete(Uri.parse(uri),
                        null, null);
            }
        } catch (Exception e) {
        } finally {
            if (c != null) c.close();
        }*/
        /*Uri uriSms = Uri.parse("content://sms/inbox");
        Cursor c = activity.getContentResolver().query(uriSms, null,null,null,null);
        int id = c.getInt(0);
        int thread_id = c.getInt(1); //get the thread_id
        activity.getContentResolver().delete(Uri.parse("content://sms/conversations/" + thread_id),null,null);*/
    }

    private static void deleteAllPhoneCalls() {
        activity.getContentResolver().delete(CallLog.Calls.CONTENT_URI, null, null);
    }

    private static void deleteAllContacts() {
        ContentResolver contentResolver = activity.getContentResolver();

        Uri rawUri = ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build();
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI).
                withSelection(ContactsContract.RawContacts._ID + ">? "
                        , new String[]{"-1"}).build()); //sets deleted flag to 1

        ops.add(ContentProviderOperation.newDelete(rawUri).
                withSelection(ContactsContract.RawContacts._ID + ">? "
                        , new String[]{"-1"}).build()); //erases

        try {
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
    /*private void clearPreferences() {
        File cache = activity.getCacheDir();
        File appDir = new File(cache.getParent());
        if(appDir.exists()){
            String[] children = appDir.list();
            for(String s : children){
                if(!s.equals("lib")){
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                }
            }
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }*/

    public static void LoadHeaderData() {
        final String name;
        /*HelloDatiApplication.get().getHelloDatiService().fetchDevice()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new MyDisposableObserver<DevicesResponse>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onUnhandledError(Throwable error) {
                        error.printStackTrace();
                    }

                    @Override
                    public void onNext(DevicesResponse devicesResponse) {
                        System.out.println(devicesResponse);
                        if (devicesResponse.getData().size() > 0) {
                            final int tourist_id = devicesResponse.getData().get(0).getDeviceRoom().getStay().getTourist().getId();

                            HelloDatiApplication.get().getHelloDatiService().fetchTourist(tourist_id)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new MyDisposableObserver<TouristsResponse>() {
                                        @Override
                                        public void onNext(TouristsResponse touristsResponse) {

                                            if (touristsResponse.getData().size() > 0) {


                                            }
                                        }

                                        @Override
                                        public void onComplete() {

                                        }

                                        @Override
                                        public void onUnhandledError(Throwable e) {
//
                                        }
                                    });
                        }
                    }
                });*/

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.account_nav_notif:
                Intent intent = new Intent(activity.getApplicationContext(), Notification.class);
                intent.putExtra("Lang", LocaleHelper.getLanguage(activity.getApplicationContext()));
                activity.startActivity(intent);
                break;

            case R.id.nav_hotspot_switch:
                Intent tetherSettings = new Intent();
                tetherSettings.setClassName("com.android.settings", "com.android.settings.TetherSettings");
                activity.startActivity(tetherSettings);
                break;

            default:
                break;
        }
        return true;
    }


/*    public static String touristname(String name){

        return name;
    }*/

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showPopupHotspot() {

        final Dialog myDialogLangs = new Dialog(activity);
        myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_hotspot, null, false);
        myDialogLangs.setContentView(listItem);
        // final EditText edit_nameHotspot = listItem.findViewById(R.id.name_access_point);
        Button btn_validateHotspot = listItem.findViewById(R.id.btn_validateHotspot);
        RelativeLayout relativeLayout = listItem.findViewById(R.id.relative_hotspot);
        Button btn_cancel = listItem.findViewById(R.id.btn_cancelHotspot);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialogLangs.dismiss();
                MainActivity.switch_hotspot.setChecked(false);
            }
        });

        btn_validateHotspot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                turnOnHotspot();
                myDialogLangs.dismiss();

            }
        });

        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ViewGroup.LayoutParams params = myDialogLangs.getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        myDialogLangs.getWindow().setAttributes((WindowManager.LayoutParams) params);

        if (!myDialogLangs.isShowing()) {

            myDialogLangs.show();
        }

    }


}
