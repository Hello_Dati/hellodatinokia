package com.hellodati.datiapp.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

public class HReservations extends AppCompatActivity {

    public static ImageButton btnback;
    public static Context context;
    public static RecyclerView recyclerViewHistory;
    public static State stateDAO=new State();
    public static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hreservations);
        View overlay = findViewById(R.id.linearAbout);
        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_FULLSCREEN);
        context=getApplicationContext();
        btnback = findViewById(R.id.back);
        recyclerViewHistory=findViewById(R.id.recycler_history);
        progressBar = findViewById(R.id.progress_bar_history);
        stateDAO.getAll("fr","iHistoryFragment.getReservationsHistoryData","iHistoryFragment.getReservationsHistoryDataError");
        // back btn click action
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
