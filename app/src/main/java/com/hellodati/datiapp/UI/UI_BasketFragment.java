package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_BasketFragment extends Fragment {

    public static View view;
    public static Context context;
    public static RecyclerView recyclerViewBasket;
    public static State stateDAO=new State();
    public static ProgressBar progressBar;
    public static CardView validBtn,cardUserCode;
    public static EditText userCode;

    public UI_BasketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initContent(inflater,container,savedInstanceState);
        stateDAO.getAll("fr","iBasketFragment.getBasketData","iBasketFragment.getBasketDataError");
        stateDAO.getAll("fr","iBasketFragment.getDeliveryPlacesData","iBasketFragment.getDeliveryPlacesError");

        validBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupConfirmation();
            }
        });

        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__basket, container, false);
        recyclerViewBasket=view.findViewById(R.id.recycler_basket);
        progressBar = view.findViewById(R.id.progress_bar_recycler);
        validBtn = view.findViewById(R.id.valider_btn);
        userCode= view.findViewById(R.id.user_id);
        cardUserCode=view.findViewById(R.id.card_user_code);
    }

    public static void showPopupConfirmation(){
        if (userCode.getText().toString().matches("")) {
            cardUserCode.startAnimation(shakeError());
        } else {
            if (userCode.getText().toString().length() > 4 || userCode.length() < 4) {
                cardUserCode.startAnimation(shakeError());
            }
            else {
                //popup then vvv
                //confirm ->reload || shakeError
                /*HelloDatiApplication.get().getHelloDatiService().fetchDevice()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new MyDisposableObserver<DevicesResponse>() {

                            @Override
                            public void onComplete() {
                                bindingView.progressBarRecycler.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onUnhandledError(Throwable e) {

                            }

                            @Override
                            public void onNext(DevicesResponse devicesResponse) {
                                // confirm all shopping Cart items
                                if (devicesResponse.getData().size() > 0) {
                                    int tourist_id = devicesResponse.getData().get(0).getDeviceRoom().getStay().getTourist().getId();
                                    final int tab_selected = bindingView.tabs.getSelectedTabPosition();
                                    HelloDatiApplication.get().getHelloDatiService().confirmAllShoppingCartOrder(tourist_id, user_code, tab_selected)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(Schedulers.io())
                                            .subscribe(new MyDisposableObserver<ShoppingCartResponse>() {
                                                @Override
                                                public void onNext(ShoppingCartResponse shoppingCartResponse) {
                                                    if (getActivity() instanceof MainActivity) {
                                                        ((MainActivity) getActivity()).updateShoppingNotifNum();
                                                    }
                                                    loadTab(tab_selected);
                                                    bindingView.userId.getText().clear();
                                                    bindingView.progressBarRecycler.setVisibility(View.GONE);
                                                }

                                                @Override
                                                public void onComplete() {

                                                }

                                                @Override
                                                public void onUnhandledError(Throwable e) {
                                                    bindingView.progressBarRecycler.setVisibility(View.GONE);
                                                    bindingView.cardUserCode.startAnimation(shakeError());
                                                }
                                            });
                                }
                            }
                        });*/
            }
        }
    }

    // show horizontal animation
    public static TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(1000);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

}
