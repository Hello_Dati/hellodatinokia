
package com.hellodati.datiapp.SSAPI;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.hellodati.datiapp.BuildConfig;
import com.hellodati.datiapp.SSAPI.CheckRequirements.AppStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class SSAPI extends SSAPI_MANAGER {
    public static int StatusCode;
    public static String Message;
    public Context context;
    public static HttpURLConnection urlConnection;
    public String nom_package = BuildConfig.APPLICATION_ID;
    public static SSAPI ssapi = new SSAPI();
    private SSAPI() {
    }
    public static SSAPI getInstance() {
        return ssapi;
    }
    public void submit(String param) {
        // if (AppStatus.getInstance().isOnline()){
        if (this.isWait()) {
            Log.d("khalil2 waiting send ", param);
            Log.d("khalil2 Last param", "l" + this.lastRequest_param);
            if (!this.EXIST_IN_PILE(param)) {
                this.pile.add(param);
                Log.d("khalil2 Add to pile ", param);
            } else {
                Log.e("khalil2 always in pile ", param);
            }
        } else {
            JSONObject obj = null;
            try {
                obj = new JSONObject(param);
                String uri = obj.getString("uri").toString();
                String[] a = new String[5];
                a[0] = uri;
                try {
                    String data = obj.getString("data").toString();
                    a[1] = data;
                } catch (Exception e) {
                    String data = "";
                    a[1] = data;
                }
                try {
                    String success = obj.getString("onSuccess");
                    if (success != null) {
                        a[2] = success;
                    } else {
                        a[2] = "";
                    }
                } catch (Exception e) {
                    String success = "";
                    a[2] = success;
                }
                try {
                    String error = obj.getString("onError");
                    if (error != null) {
                        a[3] = error;
                    } else {
                        a[3] = "";
                    }
                } catch (Exception e) {
                    String error = "";
                    a[3] = error;
                }
                a[4] = param;
                RetrieveFeedTask send = new RetrieveFeedTask();
                send.setParams(a);
                send.execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // }//else {
        //  ErrorManager(404, e.getMessage());
        //showPopupConnexion();
        // RetrieveFeedTask.showPopupConnexion();
        // Log.d("ssssssssssssss", "zzzzzzzzzzzzz   " + StatusCode);
        // }
    }
    private String getClassName(String fullName) {
        Integer pos = fullName.indexOf('.');
        String name = "";
        if (pos != -1) {
            name = String.copyValueOf(fullName.toCharArray(), 0, pos);
            return name;
        } else {
            return null;
        }
    }
    private String getMethodName(String fullName) {
        Integer pos = fullName.indexOf('.');
        Integer length = (fullName.length() - pos) - 1;
        String name = "";
        if (pos != -1) {
            name = String.copyValueOf(fullName.toCharArray(), pos + 1, length);
            return name;
        } else {
            return null;
        }
    }
    private void responseTreatment(JSONObject obj, String onSuccess, String onError, String allParam) {
        Method method;
        try {
            Boolean isValid = obj.getBoolean("isValid");
            Boolean session_updated = obj.getBoolean("session_updated");
            if (session_updated) {
                JSONObject session = obj.getJSONObject("session");
                this.update_params(session);
            }
            Integer eCode = obj.getInt("errorCode");
            String error = obj.getString("error");
            if (isValid) {
                JSONArray data = obj.getJSONArray("data");
                if (data.getString(0).equals("(0) Result found")) {
                    data = new JSONArray();
                }
                if (onSuccess != "") {
                    String className = this.getClassName(onSuccess);
                    String methodName = getMethodName(onSuccess);
                    if (className != null && methodName != null) {
                        try {
                            className = "com.hellodati.datiapp.Implementation." + className;
                            Method methodeName = Class.forName(className).getMethod(methodName, JSONArray.class);
                            methodeName.invoke(null, data);
                        } catch (Exception e) {
                            Log.e("SSAPI onSuccess method failed", e.toString());
                        }
                    }
                }
            } else {
                if (onError != "") {
                    if (this.SESSION_ERROR(eCode)) {
                        this.RESOLVE_SESSION(allParam, eCode);
                    } else {
                        String className = this.getClassName(onError);
                        String methodName = getMethodName(onError);
                        if (className != null && methodName != null) {
                            try {
                                className = "com.hellodati.datiapp.Implementation." + className;
                                Method methodeName = Class.forName(className).getMethod(methodName, int.class, String.class);
                                methodeName.invoke(null, eCode, error);
                            } catch (Exception e) {
                                Log.e("SSAPI onError method failed", e.toString());
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Boolean isValid = response.getJSONObject(0).getString();
    }
    public static class RetrieveFeedTask extends AsyncTask<Void, Void, String> {
        public String data;
        private String uri;
        private String onSuccess;
        private String onError;
        private String allParam;
        public StringBuilder response2 = new StringBuilder();
        public void setParams(String[] paramsA) {
            this.uri = paramsA[0];
            this.data = paramsA[1];
            this.onSuccess = paramsA[2];
            this.onError = paramsA[3];
            this.allParam = paramsA[4];
        }
        protected void onPreExecute() {
            //progressBar.setVisibility(View.VISIBLE);
            //responseView.setText("");
        }
        protected String doInBackground(Void... urls) {
            //String email = emailText.getText().toString();
            // Do some validation here
            try {
                URL url = new URL(SSAPI.getInstance().getDNS() + "" + this.uri);
                String urlParameters = this.data;
                byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                int postDataLength = postData.length;
                urlConnection = (HttpURLConnection) url.openConnection();
                if (SSAPI.ssapi.getSESSION() == "new") {
                    SSAPI.ssapi.lastRequest_param = allParam;
                    SSAPI.ssapi.setWait(true);
                    urlConnection.setRequestProperty("session", SSAPI.ssapi.getSESSION());
                    urlConnection.setRequestProperty("appid", SSAPI.ssapi.getAID());
                    urlConnection.setRequestProperty("app_secret", SSAPI.ssapi.getAS());
                    Log.d("khalil2 send with new", this.allParam);
                } else if (SSAPI.ssapi.getSESSION() == "linked") {
                    urlConnection.setRequestProperty("session", SSAPI.ssapi.getSESSION());
                    urlConnection.setRequestProperty("session_id", SSAPI.ssapi.getSI());
                    urlConnection.setRequestProperty("session_access_token", SSAPI.ssapi.getSAT());
                    Log.d("khalil2 send with linked", this.allParam);
                } else {
                    urlConnection.setRequestProperty("session", SSAPI.ssapi.getSESSION());
                    urlConnection.setRequestProperty("session_id", SSAPI.ssapi.getSI());
                    urlConnection.setRequestProperty("session_access_token", SSAPI.ssapi.getSAT());
                    urlConnection.setRequestProperty("session_refresh_token", SSAPI.ssapi.getSRT());
                    Log.d("khalil2 send with refresh", this.allParam);
                }
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("charset", "utf-8");
                urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                urlConnection.connect();
                try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                    wr.write(postData);
                    //Log.d("errorcode",urlConnection.getResponseCode()+" ceci");
                }
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    this.response2 = response;
//                ssapi.responseTreatment(new JSONObject(response.toString()), this.onSuccess, this.onError, this.allParam);
                    return null;
                } finally {
                    //StatusCode = urlConnection.getResponseCode();
                    // Log.d("ssssssssssssss", "sssssssssssss   " + getStatus());
                    //Log.d("ssssssssssssss", "zzzzzzzzzzzzz   " + StatusCode);
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                // ErrorManager(404, e.getMessage());
                //ErrorManager(404, e.getLocalizedMessage());
                //Log.e("ERRORExcep", StatusCode + "");
            }
            return null;
        }
        protected void ErrorManager(int statusCode, String descCode) {
            int cpt = 0;
            if (descCode.indexOf("Unable to resolve host") != -1) {
                StatusCode = 404;
                SSAPI.getInstance().wait = false;
                //  checkConnection(context);
                while (cpt < 3){
                    SSAPI.ssapi.submit(allParam);
                    cpt = cpt+1;
                    Log.d("compteur",cpt+"");
                }
            } else {
                StatusCode = 404;
            }
        }
        protected void onPostExecute(String response) {
            try {
                ssapi.responseTreatment(new JSONObject(this.response2.toString()), this.onSuccess, this.onError, this.allParam);
            } catch (JSONException e) {
                e.printStackTrace();
            }
/*            if (response == null) {
                response = "THERE WAS AN ERROR";
            }*/
        }
        public void checkConnection(Context context) {
            if (AppStatus.getInstance().isOnline()) {
                Log.v("Home", "online");
            } else {
                Log.v("Home", "offline");
            }
        }
        /*public static void showCustomPopupMenu()
        {
            WindowManager windowManager2 = (WindowManager)HelloDatiApplication.getContext().getSystemService(WINDOW_SERVICE);
            LayoutInflater layoutInflater=(LayoutInflater)HelloDatiApplication.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view=layoutInflater.inflate(R.layout.popup_connexion, null);
            WindowManager.LayoutParams params=new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.gravity= Gravity.CENTER|Gravity.CENTER;
            params.x=0;
            params.y=0;
            windowManager2.addView(view, params);
        }*/
    }
    /*public void showPopupConnexion(){
        AlertDialog.Builder builder =new AlertDialog.Builder(MainActivity.context);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }*/
}