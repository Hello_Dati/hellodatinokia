package com.hellodati.datiapp.UI;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_ReviewsFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView recyclerReviews;
    public static RelativeLayout emptyBasket;
    public static NestedScrollView scrollReview;
    public static CardView btnAvis;
    public static RelativeLayout btnAvisNotEmpty;

    public static State stateDAO=new State();

    public UI_ReviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        stateDAO.getAll("fr","iReviewsAdapter.getReviewsData","iReviewsAdapter.getReviewsDataError");
        btnAvis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });
        btnAvisNotEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });
        return view;
    }

    private void showPopup(){
        final Dialog myDialogLangs = new Dialog(context);
        myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View listItem = LayoutInflater.from(context).inflate(R.layout.popup_reviews, null, false);
        myDialogLangs.setContentView(listItem);
        Button btn_cancel = listItem.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialogLangs.dismiss();
            }
        });
        final EditText edit_text_comment = listItem.findViewById(R.id.editText_Comment);
        Button btn_send = listItem.findViewById(R.id.btn_sendReview);
        final RatingBar ratingBarReview = listItem.findViewById(R.id.ratingBarReview);
        RelativeLayout relativeLayout = listItem.findViewById(R.id.relative_review);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Review added", Snackbar.LENGTH_LONG).show();
                myDialogLangs.dismiss();
            }
        });
        myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ViewGroup.LayoutParams params = myDialogLangs.getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        myDialogLangs.getWindow().setAttributes((WindowManager.LayoutParams) params);
        myDialogLangs.show();
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui_reviews, container, false);
        recyclerReviews=view.findViewById(R.id.recycler_reviews);
        emptyBasket=view.findViewById(R.id.empty_basket);
        scrollReview=view.findViewById(R.id.scroll_review);
        btnAvis=view.findViewById(R.id.btn_avis);
        btnAvisNotEmpty=view.findViewById(R.id.btn_avis_not_empty);
    }

}
