package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hellodati.datiapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder>{

    JSONArray data;
    Context context;

    public ReviewsAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_review, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.touristName.setText(data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*try {
            viewHolder.restoName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.restoSpecialty.setText(data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_RestaurantFragment.position=1;
                UI_RestaurantFragment.tabs.setVisibility(View.VISIBLE);
                UI_RestaurantFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_RestaurantFragment.setFragmentOfTab(new UI_MenuFragment());
                try {
                    UI_RestaurantFragment.restaurantTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        /*TextView title;
        ImageView imageView;*/
        TextView touristName;

        public ViewHolder(View itemView) {
            super(itemView);
            touristName=itemView.findViewById(R.id.tourist_name);
            /*itemCard=itemView.findViewById(R.id.item_card);
            restoName=itemView.findViewById(R.id.resto_name);
            restoSpecialty=itemView.findViewById(R.id.resto_speciality);*/

        }
    }
}