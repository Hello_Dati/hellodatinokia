package com.hellodati.datiapp.UI;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hellodati.datiapp.Adapter.AppsAdapter;
import com.hellodati.datiapp.HelloDatiApplication;
import com.hellodati.datiapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_AppsFragment extends Fragment {

    public static View view;
    public static Context context;
    public static RecyclerView recyclerApps;

    public UI_AppsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        recyclerApps.setLayoutManager(new CostumGridLayoutManager(context, 4));
        AppsAdapter adapter = new AppsAdapter(GetAppsList(), context);
        recyclerApps.setAdapter(adapter);
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__apps, container, false);
        recyclerApps=view.findViewById(R.id.recycler_apps);
    }

    public ArrayList<AppInfo> GetAppsList() {
        List<String> allowedApps = null;
        if(MainActivity.isConnectedWifi(context)){
            allowedApps= Arrays.asList(HelloDatiApplication.getAllowPackagesForShow());
        }
        else {
            if (MainActivity.isConnectedMobile(context)){
                if(MainActivity.expiredPackage==0){
                    allowedApps= Arrays.asList(HelloDatiApplication.getAllowPackagesForShow());
                }
                else{
                    allowedApps= Arrays.asList(HelloDatiApplication.getAllowPackagesForShowWhenExpiredPackage());
                }
            }
        }
        PackageManager pm = context.getPackageManager();
        //new add
        ArrayList<AppInfo> appListSpec = new ArrayList<AppInfo>();
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> allApps = pm.queryIntentActivities(i, 0);
        for (ResolveInfo ri : allApps) {
            AppInfo app = new AppInfo();
            app.label = ri.loadLabel(pm).toString();
            app.packageName = ri.activityInfo.packageName;
            app.icon = ri.activityInfo.loadIcon(pm);
            if (!app.packageName.equals(context.getPackageName())) {
                if (HelloDatiApplication.isKioskModeOn()) {
                    if (allowedApps.contains(app.packageName)) {
                        appListSpec.add(app);
                    }
                } else {
                    appListSpec.add(app);
                }
            }
        }
        return appListSpec;
    }

}
