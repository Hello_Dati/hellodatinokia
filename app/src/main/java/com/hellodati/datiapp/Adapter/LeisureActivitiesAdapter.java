package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.UI_AboutFragment;
import com.hellodati.datiapp.UI.UI_LeisureFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class LeisureActivitiesAdapter extends RecyclerView.Adapter<LeisureActivitiesAdapter.ViewHolder>{

    JSONArray data;
    Context context;


    public LeisureActivitiesAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_leisure_activity, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.leisureName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.leisureTarif.setText(data.getJSONObject(i).getString("id"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_LeisureFragment.position=1;
                UI_LeisureFragment.tabs.setVisibility(View.VISIBLE);
                UI_LeisureFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_LeisureFragment.tabs.getTabAt(0).select();
                UI_LeisureFragment.setFragmentOfTab(new UI_AboutFragment());
                try {
                    UI_LeisureFragment.leisureTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        /*try {
            viewHolder.restoName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.restoSpecialty.setText(data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_RestaurantFragment.position=1;
                UI_RestaurantFragment.tabs.setVisibility(View.VISIBLE);
                UI_RestaurantFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_RestaurantFragment.tabs.getTabAt(0).select();
                UI_RestaurantFragment.setFragmentOfTab(new UI_MenuFragment());
                try {
                    UI_RestaurantFragment.restaurantTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView itemCard;
        TextView leisureName,leisureTarif;


        public ViewHolder(View itemView) {
            super(itemView);
            leisureName=itemView.findViewById(R.id.leisure_name);
            leisureTarif=itemView.findViewById(R.id.leisure_tarif);
            itemCard=itemView.findViewById(R.id.item_card);

        }
    }
}