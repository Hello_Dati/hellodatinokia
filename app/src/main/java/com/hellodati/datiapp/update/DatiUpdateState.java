package com.hellodati.datiapp.update;

import android.content.Context;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class DatiUpdateState {

    public static final String TAG = "DatiUpdateState";
    public static final int STATE_IDLE = 0,
            STATE_DOWNLOADING = 1,
            STATE_INSTALLING = 2;

    public static void setState(Context context, int state) {
        Log.i(TAG, "update_state " + state);
        context.getSharedPreferences("_", MODE_PRIVATE).edit().putInt("update_state", state).apply();
    }

    public static int getState(Context context) {
        int state = context.getSharedPreferences("_", MODE_PRIVATE).getInt("update_state", STATE_IDLE);
        Log.i(TAG, "get_state " + state);
        return state;
    }
}
