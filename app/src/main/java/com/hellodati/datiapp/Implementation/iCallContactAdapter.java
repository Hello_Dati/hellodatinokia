package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.hellodati.datiapp.Adapter.CallContactAdapter;
import com.hellodati.datiapp.UI.ContactListFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iCallContactAdapter {

    public static void getContactData(final JSONArray data) throws JSONException {
        ContactListFragment.recyclerContactList.setLayoutManager(new LinearLayoutManager(ContactListFragment.context));
        CallContactAdapter adapter = new CallContactAdapter(data, ContactListFragment.context);
        ContactListFragment.recyclerContactList.setAdapter(adapter);
    }


    public static void getContactDataError(int errorCode, String errorMSG){
        Log.e("ContactAdapter : get contact data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

}
