package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_DrinksFragment extends Fragment implements TabLayout.BaseOnTabSelectedListener {

    public View view;
    public  static TabLayout tabs;
    public  static ImageButton btnBack;
    public  static TextView serviceTitle;
    public static FrameLayout tabContent;
    public static Context context;
    public static int position;

    public UI_DrinksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        serviceTitle.setText(UI_ServiceFragment.selectedService);
        tabs.addOnTabSelectedListener(this);
        setFragmentOfTab(new UI_MenuFragment());
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position){
                    case 0:
                        UI_ServiceFragment.frameLayout.removeAllViews();
                        break;
                    case 1:
                        setFragmentOfTab(new UI_MenuFragment());
                        break;
                }

            }
        });
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__drinks, container, false);
        serviceTitle=view.findViewById(R.id.service_title);
        btnBack=view.findViewById(R.id.btn_back);
        tabs=view.findViewById(R.id.tabs);
        tabContent=view.findViewById(R.id.tab_content);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Fragment fragment = null;
        switch (tab.getPosition()) {
            case 0:
                // menu
                //fragment = HotelHorizontalMenuFragment.getWithParams(article);
                fragment = new UI_MenuFragment();
                break;
            case 1:
                // reviews
                fragment = new UI_ReviewsFragment();
                break;
            default:
                fragment = null;
                break;
        }
        setFragmentOfTab(fragment);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    // set the data in the content place
    public static void setFragmentOfTab(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = ((AppCompatActivity) UI_ServiceFragment.context).getSupportFragmentManager().beginTransaction();
            ft.replace(tabContent.getId(), fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}
