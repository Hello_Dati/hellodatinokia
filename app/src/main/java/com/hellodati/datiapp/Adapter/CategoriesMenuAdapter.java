package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellodati.datiapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class CategoriesMenuAdapter extends RecyclerView.Adapter<CategoriesMenuAdapter.ViewHolder> {

    JSONArray data;
    Context context;


    public CategoriesMenuAdapter(JSONArray data, Context context) {
        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_menu_category, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.categoryName.setText(data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*try {
            viewHolder.leisureOffersName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.leisureOffersPromo.setText("-"+data.getJSONObject(i).getString("id")+"%");
            viewHolder.leisureOffersTarif.setText(data.getJSONObject(i).getString("id"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_LeisureFragment.position = 1;
                UI_LeisureFragment.tabs.setVisibility(View.VISIBLE);
                UI_LeisureFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_LeisureFragment.tabs.getTabAt(0).select();
                UI_LeisureFragment.setFragmentOfTab(new UI_AboutFragment());
                try {
                    UI_LeisureFragment.leisureTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout categoryLayout;
        TextView categoryName;


        public ViewHolder(View itemView) {
            super(itemView);
            categoryLayout=itemView.findViewById(R.id.category_layout);
            categoryName=itemView.findViewById(R.id.category_name);

        }
    }
}

