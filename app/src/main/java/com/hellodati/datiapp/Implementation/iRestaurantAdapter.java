package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;

import com.hellodati.datiapp.Adapter.RestaurantAdapter;
import com.hellodati.datiapp.UI.UI_RestaurantListFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iRestaurantAdapter {

    public static void getRestaurantListData(JSONArray data) throws JSONException {
        UI_RestaurantListFragment.restoRecyclerView.setLayoutManager(new LinearLayoutManager(UI_RestaurantListFragment.context));
        RestaurantAdapter adapter = new RestaurantAdapter(data, UI_RestaurantListFragment.context);
        UI_RestaurantListFragment.restoRecyclerView.setAdapter(adapter);
    }

    public static void getRestaurantListDataError(int errorCode, String errorMSG){

    }

}
