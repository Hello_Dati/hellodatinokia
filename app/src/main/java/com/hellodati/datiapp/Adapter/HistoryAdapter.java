package com.hellodati.datiapp.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellodati.datiapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{
    JSONArray data;
    Context context;



    public HistoryAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_history, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {


        try {

            if (i%2 ==0){
                viewHolder.relativeLayout.setBackgroundResource(R.color.white);
            }else {
                viewHolder.relativeLayout.setBackgroundResource(R.color.offwhite);
            }
            viewHolder.commandeName.setText("24-07-2020");

            viewHolder.dateOfCommande.setText(data.getJSONObject(i).getString("name"));


            viewHolder.totalPrice.setText("230 Dt");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView commandeName,dateOfCommande,totalPrice;

        RelativeLayout relativeLayout;

        public ViewHolder(final View itemView) {
            super(itemView);
            commandeName = itemView.findViewById(R.id.title_history);
            dateOfCommande = itemView.findViewById(R.id.date_history);
            totalPrice = itemView.findViewById(R.id.price_history);
            relativeLayout = itemView.findViewById(R.id.item_purchase);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
