package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_MeetingListFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView meetRecyclerView;

    public static State stateDAO=new State();

    public UI_MeetingListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        UI_MeetingFragment.position=0;
        UI_MeetingFragment.tabs.setVisibility(View.GONE);
        UI_MeetingFragment.buttomHeader.setVisibility(View.GONE);
        stateDAO.getAll("fr","iMeetAdapter.getMeetListData","iMeetAdapter.getMeetListDataError");
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui__meeting_list, container, false);
        meetRecyclerView=view.findViewById(R.id.meet_recycler_view);
    }

}
