package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;

import com.hellodati.datiapp.Adapter.WellBeingActivitiesAdapter;
import com.hellodati.datiapp.Adapter.WellBeingOffersAdapter;
import com.hellodati.datiapp.UI.UI_WellBeingListFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iWellBeingAdapter {

    public static void getWellBeingActivitiesListData(JSONArray data) throws JSONException {
        UI_WellBeingListFragment.recyclerWellBeingActivites.setLayoutManager(new LinearLayoutManager(UI_WellBeingListFragment.context));
        WellBeingActivitiesAdapter adapter = new WellBeingActivitiesAdapter(data, UI_WellBeingListFragment.context);
        UI_WellBeingListFragment.recyclerWellBeingActivites.setAdapter(adapter);
    }

    public static void getWellBeingActivitiesListDataError(int errorCode, String errorMSG){

    }

    public static void getWellBeingOffersListData(JSONArray data) throws JSONException {
        LinearLayoutManager lm = new LinearLayoutManager(UI_WellBeingListFragment.context);
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        UI_WellBeingListFragment.recyclerWellBeingOffers.setLayoutManager(lm);
        WellBeingOffersAdapter adapter = new WellBeingOffersAdapter(data, UI_WellBeingListFragment.context);
        UI_WellBeingListFragment.recyclerWellBeingOffers.setAdapter(adapter);
    }

    public static void getWellBeingOffersListDataError(int errorCode, String errorMSG){

    }
}
