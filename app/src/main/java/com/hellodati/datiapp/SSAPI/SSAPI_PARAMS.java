package com.hellodati.datiapp.SSAPI;

/**
 * © Kamatcho, 04/07/2020 -  My Application.
 */
public class SSAPI_PARAMS {
    public String onSuccess="";
    public String onError="";
    private String POSTS="";
    private String GETS="";
    public String URI="";

    public String getGETS() {
        return GETS;
    }

    public String getPOSTS() {
        return POSTS;
    }

    public String getJson(){
        String s = "";
        if(this.GETS == ""){
            s = "{uri:'"+this.URI+"'";
        }else{
            s = "{uri:'"+this.URI+"/"+this.getGETS()+"'";
        }

        if(this.POSTS != ""){
            s = s+",data:"+this.POSTS;
        }

        if(this.onSuccess != ""){
            s = s+",onSuccess:"+this.onSuccess;
        }
        if(this.onError != ""){
            s = s+",onError:"+this.onError;
        }

        s = s+"}";
        return s;

    }
    public void addPost(String key, String value){
        String s = key+"="+value;
        if(this.POSTS == ""){
            this.POSTS= "'"+s+"'";
        }else{
            String s2 =this.POSTS;
            Integer length = this.POSTS.length()-1;
            this.POSTS = String.copyValueOf(s2.toCharArray(), 0, length);
            this.POSTS = this.POSTS+","+key+"="+value+"'";
        }
    }
    public void addGets(String key, String value){
        String s = key+"="+value;
        if(this.GETS == ""){
            this.GETS= s;
        }else{
            String s2 =this.GETS;
            this.GETS = this.GETS+"/"+key+"="+value;
        }
    }
}
