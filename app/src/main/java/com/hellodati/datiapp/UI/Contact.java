package com.hellodati.datiapp.UI;

public class Contact {
    private String label = "",
            phone = "";
    private int image = 0;
    CallOption group = null;

    public Contact(String label, String phone, CallOption group) {
        setLabel(label);
        setPhone(phone);
        setGroup(group);
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CallOption getGroup() {
        return group;
    }

    public void setGroup(CallOption group) {
        this.group = group;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}