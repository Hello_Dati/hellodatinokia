package com.hellodati.datiapp.UI;

public class CallOption {
    private static final String TAG = "CallOption";

    public enum CallOptionCode {
        ROOM,
        NATIONAL,
        INTERNATIONAL,
        EMERGENCY,
        ROOM_SERVICE
    }
    
    public CallOptionCode mCallOptionCode;
    public String mTitle, mSummery, mDescription;
    public int mIcon;
    public int mColor;
    public String mImg;

    public CallOption(CallOptionCode callOptionCode, String title, int icon, String summery, String description, int color) {
        mCallOptionCode = callOptionCode;
        mIcon = icon;
        mTitle = title;
        mSummery = summery;
        mDescription = description;
        mColor = color;
    }

    public CallOption(CallOptionCode mCallOptionCode, String mTitle, String mSummery, String mDescription, int mIcon, String mImg) {
        this.mCallOptionCode = mCallOptionCode;
        this.mTitle = mTitle;
        this.mSummery = mSummery;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
        this.mImg = mImg;
    }
}