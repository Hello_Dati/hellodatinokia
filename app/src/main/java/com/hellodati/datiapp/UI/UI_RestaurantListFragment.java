package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_RestaurantListFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView restoRecyclerView;
    public static SwipeRefreshLayout swipeContainer;
    public static State stateDAO=new State();

    public UI_RestaurantListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        UI_RestaurantFragment.position=0;
        UI_RestaurantFragment.tabs.setVisibility(View.GONE);
        UI_RestaurantFragment.buttomHeader.setVisibility(View.GONE);
        stateDAO.getAll("fr","iRestaurantAdapter.getRestaurantListData","iRestaurantAdapter.getRestaurantListDataError");

        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui_restaurant_list, container, false);
        restoRecyclerView=view.findViewById(R.id.resto_recycler_view);
        swipeContainer=view.findViewById(R.id.swipe_container);
    }

}
