package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.LocaleHelper;
import com.hellodati.datiapp.UI.MainActivity;
import com.hellodati.datiapp.UI.UI_CallFragment;

import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class CallContactAdapter extends RecyclerView.Adapter<CallContactAdapter.ViewHolder> {

    JSONArray data;
    Context context;

    public CallContactAdapter(JSONArray data, Context context) {
        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_contact, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.contactNumber.setText(data.getJSONObject(i).getString("name"));
            viewHolder.contactNumber1.setText(data.getJSONObject(i).getString("id"));
            viewHolder.contactColor.setCardBackgroundColor(UI_CallFragment.selectedOption.mColor);
            viewHolder.contactIcon.setImageResource(UI_CallFragment.selectedOption.mIcon);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent phoneCall= context.getPackageManager().getLaunchIntentForPackage("com.datiphone");
                switch (UI_CallFragment.selectedOption.mCallOptionCode){
                    case ROOM:
                    case EMERGENCY:
                        phoneCall.putExtra("lang", LocaleHelper.getLanguage(context));
                        try {
                            phoneCall.putExtra("number", data.getJSONObject(i).getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        phoneCall.putExtra("limit", MainActivity.call_limit);
                        phoneCall.putExtra("call_time",  MainActivity.call_time);
                        phoneCall.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(phoneCall);
                    case INTERNATIONAL:{
                        phoneCall.putExtra("lang", LocaleHelper.getLanguage(context));
                        try {
                            phoneCall.putExtra("prefix_number", "+"+data.getJSONObject(i).getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        phoneCall.putExtra("limit", MainActivity.call_limit);
                        phoneCall.putExtra("call_time",  MainActivity.call_time);
                        phoneCall.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(phoneCall);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView contactColor,itemCard;
        ImageView contactIcon;
        TextView contactNumber,contactNumber1;

        public ViewHolder(View itemView) {
            super(itemView);
            itemCard=itemView.findViewById(R.id.item_card);
            contactColor=itemView.findViewById(R.id.contact_color);
            contactIcon=itemView.findViewById(R.id.contact_icon);
            contactNumber=itemView.findViewById(R.id.contact_number);
            contactNumber1=itemView.findViewById(R.id.contact_number1);

        }
    }
}