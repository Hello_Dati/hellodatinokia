package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.UI_AboutFragment;
import com.hellodati.datiapp.UI.UI_WellBeingFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class WellBeingOffersAdapter extends RecyclerView.Adapter<WellBeingOffersAdapter.ViewHolder> {

    JSONArray data;
    Context context;


    public WellBeingOffersAdapter(JSONArray data, Context context) {
        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_well_being_offer, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.wellBeingOfferName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.wellBeingOfferPromo.setText("-"+data.getJSONObject(i).getString("id")+"%");
            viewHolder.wellBeingOfferTarif.setText(data.getJSONObject(i).getString("id"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_WellBeingFragment.position = 1;
                UI_WellBeingFragment.tabs.setVisibility(View.VISIBLE);
                UI_WellBeingFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_WellBeingFragment.tabs.getTabAt(0).select();
                UI_WellBeingFragment.setFragmentOfTab(new UI_AboutFragment());
                try {
                    UI_WellBeingFragment.wellBeingTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout itemCard;
        TextView wellBeingOfferName,wellBeingOfferPromo,wellBeingOfferTarif;


        public ViewHolder(View itemView) {
            super(itemView);
            wellBeingOfferName=itemView.findViewById(R.id.well_being_offer_name);
            wellBeingOfferPromo=itemView.findViewById(R.id.well_being_offer_promo);
            wellBeingOfferTarif=itemView.findViewById(R.id.well_being_offer_tarif);
            itemCard=itemView.findViewById(R.id.item_card);

        }
    }
}