package com.hellodati.datiapp.SSAPI.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hotel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("stars")
    @Expose
    private Integer stars;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("youtube")
    @Expose
    private String youtube;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("trip_advisor_url")
    @Expose
    private String tripAdvisorUrl;
    @SerializedName("root_post_id")
    @Expose
    private Object rootPostId;
    @SerializedName("created_at")
    @Expose
    private Integer createdAt;
    @SerializedName("updated_at")
    @Expose
    private Integer updatedAt;
    @SerializedName("check_in")
    @Expose
    private Integer checkIn;
    @SerializedName("check_out")
    @Expose
    private Integer checkOut;

    @SerializedName("adress_print")
    @Expose
    private String adressPrint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTripAdvisorUrl() {
        return tripAdvisorUrl;
    }

    public void setTripAdvisorUrl(String tripAdvisorUrl) {
        this.tripAdvisorUrl = tripAdvisorUrl;
    }

    public Integer getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Integer checkIn) {
        this.checkIn = checkIn;
    }

    public Integer getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Integer checkOut) {
        this.checkOut = checkOut;
    }

    public String getAdressPrint() {
        return adressPrint;
    }

    public void setAdressPrint(String adressPrint) {
        this.adressPrint = adressPrint;
    }

    public Object getRootPostId() {
        return rootPostId;
    }

    public void setRootPostId(Object rootPostId) {
        this.rootPostId = rootPostId;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
    }
}
