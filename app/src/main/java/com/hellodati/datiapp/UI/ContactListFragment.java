package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactListFragment extends Fragment {

    public View view;
    public static Context context;
    public static CardView cardColor,header;
    public static RecyclerView recyclerContactList;
    public static ImageView cardIcon;
    public static TextView cardTitle,cardSummery;
    public static ImageButton btnBack;

    public ContactListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        cardColor.setBackgroundColor(UI_CallFragment.selectedOption.mColor);
        header.setBackgroundColor(UI_CallFragment.selectedOption.mColor);
        cardTitle.setText(UI_CallFragment.selectedOption.mTitle);
        cardSummery.setText(UI_CallFragment.selectedOption.mSummery);
        cardIcon.setImageResource(UI_CallFragment.selectedOption.mIcon);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_CallFragment.detailed.removeAllViews();
                UI_CallFragment.listOption.setVisibility(View.VISIBLE);
                UI_CallFragment.detailed.setVisibility(View.GONE);
            }
        });

        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_contact_list, container, false);
        cardColor=view.findViewById(R.id.card_color);
        recyclerContactList=view.findViewById(R.id.recycler_contact_list);
        cardIcon=view.findViewById(R.id.card_icon);
        cardTitle=view.findViewById(R.id.card_title);
        cardSummery=view.findViewById(R.id.card_summery);
        btnBack=view.findViewById(R.id.btn_back);
        header=view.findViewById(R.id.header);
    }

}
