package com.hellodati.datiapp.UI;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.hellodati.datiapp.Adapter.MainPagerAdapter;
import com.hellodati.datiapp.AdminReceiver;
import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.HelloDatiApplication;
import com.hellodati.datiapp.R;
import com.hellodati.datiapp.update.DatiUpdateState;
import com.hellodati.datiapp.update.DownloadUpdate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private FrameLayout mLoading;
    private NonSwipableViewPager mViewPager;
    private BottomNavigationView mNavigation;
    NavigationView navigationView;
    private DrawerLayout mDrawer;
    private List<Integer> PagesRes;
    private boolean isFirstLoad;
    public View header;
    //public static FragmentHotelBinding bindingView;
    public static TextView badge_nav_notif;
    public static SwitchCompat switch_hotspot;
    public TextView profileName;
    public CircleImageView photo_profile;
    public static int lastCallTime = 0;
    public static long total = (TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes())/1000;
    public static int expiredPackage = 1;
    public static int checkOut;//getCheckOut value from API

    public static long data_time=0;//getDataTime value from API
    public static long data_limit=0;//getDataLimit value from API
    public static long call_time;//getDataTime value from API
    public static long call_limit;//getDataLimit value from API

    public static State stateDAO=new State();
    public static JSONArray notificationData=null;

    //Request To API every 2 seconds
    Handler handler = new Handler();
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            JSONObject notification;
            stateDAO.getAll("fr","iMainActivity.getNotificationData","iMainActivity.getNotificationDataError");
            AccountFragment.initializeCountDrawer();
            if(notificationData!=null){
                try {
                    notification=notificationData.getJSONObject(11);
                    wakeUp();
                    switch (notification.getString("name")){
                        //information
                        case "Al Khobar":{
                            NotificationTask send = new NotificationTask();
                            send.execute();
                            break;
                        }
                        //advertisement
                        case "al Bahah":{
                            //image pub
                            if(Integer.parseInt(notification.getString("id"))==1){
                                new Thread()
                                {
                                    public void run()
                                    {
                                        activity.runOnUiThread(new Runnable()
                                        {
                                            public void run()
                                            {
                                                final Dialog myDialogLangs = new Dialog(activity);
                                                myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                final View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_pub, null, false);
                                                myDialogLangs.setContentView(listItem);
                                                myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                                final ViewGroup.LayoutParams params = myDialogLangs.getWindow().getAttributes();
                                                final RelativeLayout imagePub = listItem.findViewById(R.id.image_pub);
                                                final VideoView videoPub = listItem.findViewById(R.id.video_pub);
                                                final ImageView pubImage = listItem.findViewById(R.id.pub_image);
                                                final TextView flashPromos = listItem.findViewById(R.id.flash_promos);
                                                final TextView flashPrice = listItem.findViewById(R.id.flash_price);
                                                final TextView flashPeso = listItem.findViewById(R.id.flash_peso);
                                                final TextView title = listItem.findViewById(R.id.title);
                                                final TextView summery = listItem.findViewById(R.id.summery);
                                                final ProgressBar progressBar=listItem.findViewById(R.id.pb);
                                                videoPub.setVisibility(View.GONE);
                                                imagePub.setVisibility(View.VISIBLE);
                                                progressBar.setProgressTintList(ColorStateList.valueOf(activity.getColor(R.color.progress_color)));
                                                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                                                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                                                myDialogLangs.getWindow().setAttributes((WindowManager.LayoutParams) params);
                                                flashPeso.setText(activity.getString(R.string.shopping_currency));
                                                title.setText("azerty");
                                                summery.setText("azerty azerty azerty azerty azerty azerty azerty azerty");
                                                flashPrice.setText(50+"");
                                                //flashPromos.setText("-"+Math.round(post.getPricePromo())+"%");
                                                flashPromos.setText("-"+35+"%");
                                                pubImage.setImageResource(R.drawable.flag_tn);
                                                myDialogLangs.setCancelable(false);
                                                //Glide.with(activity.getApplicationContext()).load(post.getImage()).into(pubImage);
                                                final CountDownTimer countDownTimer=new CountDownTimer(20000,100) {
                                                    int progress=0;
                                                    @Override
                                                    public void onTick(long millisUntilFinished) {
                                                        progress+=1;
                                                        progressBar.setProgress(progress);
                                                        myDialogLangs.show();
                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        myDialogLangs.dismiss();
                                                    }
                                                };
                                                countDownTimer.start();
                                            }
                                        });
                                    }
                                }.start();
                            }
                            //video pub
                            else if(Integer.parseInt(notification.getString("id"))==2){
                                new Thread()
                                {
                                    public void run()
                                    {
                                        activity.runOnUiThread(new Runnable()
                                        {
                                            public void run()
                                            {
                                                final Dialog myDialogLangs = new Dialog(activity);
                                                myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                final View listItem = LayoutInflater.from(activity).inflate(R.layout.popup_pub, null, false);
                                                myDialogLangs.setContentView(listItem);
                                                myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                                final ViewGroup.LayoutParams params = myDialogLangs.getWindow().getAttributes();
                                                final RelativeLayout imagePub = listItem.findViewById(R.id.image_pub);
                                                final VideoView videoPub = listItem.findViewById(R.id.video_pub);
                                                final ImageView pubImage = listItem.findViewById(R.id.pub_image);
                                                final TextView flashPromos = listItem.findViewById(R.id.flash_promos);
                                                final TextView flashPrice = listItem.findViewById(R.id.flash_price);
                                                final TextView flashPeso = listItem.findViewById(R.id.flash_peso);
                                                final TextView title = listItem.findViewById(R.id.title);
                                                final TextView summery = listItem.findViewById(R.id.summery);
                                                final ProgressBar progressBar=listItem.findViewById(R.id.pb);
                                                videoPub.setVisibility(View.VISIBLE);
                                                imagePub.setVisibility(View.GONE);
                                                myDialogLangs.setCancelable(false);
                                                videoPub.setVideoURI(Uri.parse("https://hellodati.com/img/demo2.mp4"));
                                                videoPub.start();
                                                myDialogLangs.show();
                                                final CountDownTimer countDownTimer=new CountDownTimer(20000,100) {
                                                    @Override
                                                    public void onTick(long millisUntilFinished) {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        myDialogLangs.dismiss();
                                                    }
                                                };
                                                countDownTimer.start();
                                            }
                                        });
                                    }
                                }.start();
                            }
                            break;
                        }
                        //update
                        case "Al Riyadh":{
                            //get apk URL from API
                            UpdateTask send = new UpdateTask();
                            send.setParams("apkURL");
                            send.execute();
                            break;
                        }
                        //check-out
                        case "Aseer":{
                            Intent intent = new Intent(activity, UI_welcomeActivity.class);
                            activity.startActivity(intent);
                            activity.finish();
                            break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            handler.postDelayed(this, 40000);
        }
    };

    Bundle savedInstanceState;
    ImageView btn_tripadvisor;
    public RelativeLayout clearData;
    public static Activity activity;

    Dialog myDialog;
    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    Button edite_profile ;
    public String guestNames, roomNumber;
    public int versionCodes;
    Typeface weatherFont;
    String city = "Tunis, TN";
    // String OPEN_WEATHER_MAP_API;
    ImageView img_profile;

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHash;

    void initContents() {
        ArrayList<Fragment> mFragmentList = new ArrayList<>();
        mFragmentList.add(new UI_GuideFragment());
        mFragmentList.add(new UI_AppsFragment());
        mFragmentList.add(new UI_ServiceFragment());
        mFragmentList.add(new UI_CallFragment());
        mFragmentList.add(new UI_BasketFragment());
        //mFragmentList.add(new BasketFragment());
        mViewPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager(), mFragmentList));
        mViewPager.addOnPageChangeListener(this);
        mNavigation.setOnNavigationItemSelectedListener(this);
        mNavigation.setSelectedItemId(R.id.navigation_hotel);
        navigationView = findViewById(R.id.account_nav_view);
        header = navigationView.getHeaderView(0);
        edite_profile = header.findViewById(R.id.btn_edit_profile);
    }

    /*@Override
    protected void onReady() {
        if (!isFirstLoad) return;
        initContents();
        isFirstLoad = false;
        setupProfile();
        updateShoppingNotifNum();
    }

    @Override
    protected void preLoading() {

    }*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=this;
        this.savedInstanceState = savedInstanceState;
        isFirstLoad = true;
        setContentView(R.layout.activity_main);
        initView();
        initContents();
        //search for notification every 2 seconds
        handler.post(runnableCode);
        myDialog = new Dialog(MainActivity.this);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake() {
                ShowPopup();
            }
        });
        new AccountFragment(this, mDrawer);
        clearData = findViewById(R.id.clear_data);
        setLock(true);
        fullScreen();
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        //get call & data from API
        //get data info from API
        //get call info from API
        call_limit=45000;
        call_time=0;

        clearData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountFragment.ClearData();
            }
        });
        btn_tripadvisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.tripadvisor.com/Hotel_Review-g297942-d455953-Reviews-Carthage_Thalasso-Gammarth_Tunis_Governorate.html");
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
        edite_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.closeDrawers();
                Intent intent = new Intent(getApplicationContext(), Profile.class);
                intent.putExtra("Lang", LocaleHelper.getLanguage(getApplicationContext()));
                startActivity(intent);
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.READ_SMS,
                            Manifest.permission.READ_CONTACTS},
                    1);
        }

    }

    private void fullScreen() {
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        getWindow().getDecorView().setSystemUiVisibility(flags);
        final View decorView = getWindow().getDecorView();
        decorView
                .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                {

                    @Override
                    public void onSystemUiVisibilityChange(int visibility)
                    {
                        if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                        {
                            decorView.setSystemUiVisibility(flags);
                        }
                    }
                });
    }

    private void ShowPopup() {

        TextView imei, guestName, roomNum, versionApp;
        CardView btnOk;

        myDialog.setContentView(R.layout.info_device);
        btnOk = myDialog.findViewById(R.id.ok);
        imei = myDialog.findViewById(R.id.imei_device);
        roomNum = myDialog.findViewById(R.id.room_num);
        guestName = myDialog.findViewById(R.id.gest_nam);
        versionApp = myDialog.findViewById(R.id.version_app);
        //imei.setText(MainDataProvider.instance.getImei());
        roomNum.setText(roomNumber);
        guestName.setText(guestNames);
        versionApp.setText(versionCodes + "");

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });


        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //get data from DatiCall
        Intent intent = getIntent();
        Long new_call_time = intent.getLongExtra("call_time", 0);
        if (intent != null && new_call_time >= call_time) {
            call_time=new_call_time;
            //updated call time with API
        }
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mShakeDetector);
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = MainActivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = MainActivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /*void setupProfile() {
        btn_tripadvisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TripadvisorActivity.class);
                startActivity(intent);
            }
        });
    }*/

    @Override
    public void onBackPressed() {
    }


    /*@Override
    void loadDevice() {
        super.loadDevice();
        navigationView = (NavigationView) findViewById(R.id.account_nav_view);
        header = navigationView.getHeaderView(0);
        assert HelloDatiApplication.get() != null;
        HelloDatiApplication.get().getHelloDatiService().fetchDevice()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new MyDisposableObserver<DevicesResponse>() {
                    @Override
                    public void onNext(DevicesResponse devicesResponse) {
                        if (devicesResponse.getData().size() > 0) {
                            Intent intent = getIntent();
                            int old_call_time = devicesResponse.getData().get(0).getCall_time();
                            int new_call_time = intent.getIntExtra("call_time", 0);
                            if (intent != null && new_call_time >= old_call_time) {
                                int deviceID = devicesResponse.getData().get(0).getId();
                                lastCallTime = new_call_time;
                                //Log.d("Last call time 1 : ", new_call_time + "");
                                //Log.d("Last call time 2 : ", lastCallTime + "");
                                HelloDatiApplication.get().getHelloDatiService().updateDev(deviceID, new_call_time)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(new MyDisposableObserver<DevicesResponse>() {
                                            @Override
                                            public void onComplete() {
                                            }

                                            @Override
                                            public void onUnhandledError(Throwable e) {

                                            }

                                            @Override
                                            public void onNext(DevicesResponse devices) {


                                            }
                                        });
                            }
                            if (devicesResponse.getData().get(0).getDeviceRoom().getStay().getTourist().getId() != null) {
                                int touristId = devicesResponse.getData().get(0).getDeviceRoom().getStay().getTourist().getId();
                                HelloDatiApplication.get().getHelloDatiService().fetchTourist(touristId)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(new MyDisposableObserver<TouristsResponse>() {
                                            @Override
                                            public void onUnhandledError(Throwable e) {

                                            }

                                            @Override
                                            public void onNext(TouristsResponse touristsResponse) {
                                                if (touristsResponse.getData().get(0).getPwd() == null) {
                                                    Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
                                                    startActivity(intent);
                                                }
                                                profileName = header.findViewById(R.id.profile_name);
                                                photo_profile = header.findViewById(R.id.profile_image);

                                                profileName.setText(touristsResponse.getData().get(0).getLastName() + " " + touristsResponse.getData().get(0).getFirstName());
                                                if (touristsResponse.getData().get(0).getImage() != null) {
                                                    Glide.with(getApplicationContext()).load(touristsResponse.getData().get(0).getImage()).into(photo_profile);
                                                }
                                            }

                                            @Override
                                            public void onComplete() {


                                            }
                                        });
                            }
                        }
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onUnhandledError(Throwable e) {

                    }
                });

        Button edite_profile = (Button) header.findViewById(R.id.btn_edit_profile);
        edite_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.closeDrawers();
                Intent intent = new Intent(getApplicationContext(), Edite_Profile_Fragment.class);
                intent.putExtra("Lang", LocaleHelper.getLanguage(getApplicationContext()));
                startActivity(intent);
            }
        });
    }

    public void restartApp() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }*/

    void initView() {
        mNavigation = findViewById(R.id.navigation);
        mViewPager = findViewById(R.id.main_pager);
        mDrawer = findViewById(R.id.drawer_layout);
        mLoading = findViewById(R.id.loading);
        btn_tripadvisor = findViewById(R.id.btn_tripadvisor);
        PagesRes = Arrays.asList(R.id.navigation_guide, R.id.navigation_apps, R.id.navigation_hotel, R.id.navigation_call, R.id.navigation_shop);
        mDrawer = findViewById(R.id.drawer_layout);
        findViewById(R.id.img_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;



        getWindow().getDecorView().setSystemUiVisibility(flags);

        // Code below is to handle presses of Volume up or Volume down.
        // Without this, after pressing volume buttons, the navigation bar will
        // show up and won't hide
        final View decorView = getWindow().getDecorView();
        decorView
                .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                {

                    @Override
                    public void onSystemUiVisibilityChange(int visibility)
                    {
                        if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                        {
                            decorView.setSystemUiVisibility(flags);
                        }
                    }
                });
    }

    /*public void toggleLock() {
        setLock(!HelloDatiApplication.get().isKioskModeOn());
    }*/

    public void setLock(boolean enabled) {
        try {
            if (enabled) {
                ComponentName deviceAdmin = new ComponentName(this, AdminReceiver.class);
                DevicePolicyManager mDpm = (DevicePolicyManager) this.getSystemService(Context.DEVICE_POLICY_SERVICE);
                if (!mDpm.isAdminActive(deviceAdmin)) {
                    Log.e("azerty","azerty");
                    return;
                }
                if (mDpm.isDeviceOwnerApp(getPackageName())) {
                    if (MainActivity.expiredPackage == 0) {
                        mDpm.setLockTaskPackages(deviceAdmin, HelloDatiApplication.getAllowPackages());
                    }
                    if (MainActivity.expiredPackage == 1) {
                        if (isConnectedWifi(getApplicationContext())) {
                            mDpm.setLockTaskPackages(deviceAdmin, HelloDatiApplication.getAllowPackages());
                        } else {
                            mDpm.setLockTaskPackages(deviceAdmin, HelloDatiApplication.getAllowPackagesExipred());
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        mDpm.setLockTaskFeatures(deviceAdmin,
                                DevicePolicyManager.LOCK_TASK_FEATURE_HOME |
                                        //DevicePolicyManager.LOCK_TASK_FEATURE_NOTIFICATIONS|
                                        DevicePolicyManager.LOCK_TASK_FEATURE_GLOBAL_ACTIONS |
                                        DevicePolicyManager.LOCK_TASK_FEATURE_KEYGUARD |
                                        DevicePolicyManager.LOCK_TASK_FEATURE_OVERVIEW |
                                        DevicePolicyManager.LOCK_TASK_FEATURE_SYSTEM_INFO
                        );
                    }
                } else {
                    return;
                }
                if (mDpm.isLockTaskPermitted(getPackageName())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mDpm.setStatusBarDisabled(deviceAdmin, true);
                    }
                    startLockTask();
                    HelloDatiApplication.setKioskModeOn(true);
                } else {
                }
            } else {
                stopLockTask();
                HelloDatiApplication.setKioskModeOn(true);
            }
        } catch (Exception e) {
        }
    }

    int previousLoadedPage = -1;

    @Override
    public void onPageScrolled(int i, float v, int i1) {
        if (v == 0) {
            if (previousLoadedPage == i) return;
            //load fragment of  newPosition
            Fragment currentPage = getPageAt(i);
            if (currentPage instanceof BaseFragment) {
                ((BaseFragment) currentPage).onFragmentLoad();
            }
            //UNLoad fragment of  previousLoadedPage
            if (previousLoadedPage != -1) {
                currentPage = getPageAt(previousLoadedPage);
                if (currentPage instanceof BaseFragment) {
                    ((BaseFragment) currentPage).onFragmentUnload();
                }
            }
            previousLoadedPage = i;
        }
    }

    Fragment getPageAt(int position) {
        return getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.main_pager + ":" + position);
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return switchPage(PagesRes.indexOf(menuItem.getItemId()));
    }

    boolean switchPage(int pageId) {
        if (pageId == mViewPager.getCurrentItem()) {
            Fragment currentPage = getPageAt(mViewPager.getCurrentItem());
            if (currentPage instanceof BaseFragment) {
                ((BaseFragment) currentPage).onFragmentReload();
            }
            return true;
        } else {
            if (previousLoadedPage != pageId) {

                mViewPager.setCurrentItem(pageId, false);
            }
            return true;
        }
    }

    public void switchToCart() {
        mNavigation.setSelectedItemId(R.id.navigation_shop);
    }

    public void updateShoppingNotifNum() {
        /*assert HelloDatiApplication.get() != null;
        if (HelloDatiApplication.get().getDevice().getDeviceRoom().getStay().getTourist().getId() != null) {
            int touristId = HelloDatiApplication.get().getDevice().getDeviceRoom().getStay().getTourist().getId();
            HelloDatiApplication.get().getHelloDatiService().fetchDraftShoppingOrdersPurchaseReservations(touristId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new MyDisposableObserver<ShoppingCartResponse>() {
                        @Override
                        public void onNext(ShoppingCartResponse shoppingCartResponse) {
                            TextView txt_badge_notification = (TextView) findViewById(R.id.txt_badge_notification);
                            ((TextView) findViewById(R.id.txt_badge_notification)).setText(shoppingCartResponse.getData().size() + "");
                            txt_badge_notification.startAnimation(shakeError());
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onUnhandledError(Throwable e) {
                            e.printStackTrace();
                        }
                    });
        }*/
    }

    public static TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(1000);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    void networkUsage() {
        // Get running processes
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo runningApp : runningApps) {
            long received = TrafficStats.getUidRxBytes(runningApp.uid);
            long sent = TrafficStats.getUidTxBytes(runningApp.uid);
            //Log.d("test", String.format(Locale.getDefault(), "uid: %1d - name: %s: Sent = %1d, Rcvd = %1d", runningApp.uid, runningApp.processName, sent, received));
        }
    }

    protected void wakeUp() {
        PowerManager powerManager = (PowerManager) MainActivity.activity.getSystemService(POWER_SERVICE);
        if (!powerManager.isInteractive()){ // if screen is not already on, turn it on (get wake_lock for 10 seconds)
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE,"MH24_SCREENLOCK");
            wl.acquire(10000);
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MH24_SCREENLOCK");
            wl_cpu.acquire(10000);
        }
    }

    //----------------- Notification
    public static String CHANNEL_ID = "HELLO DATI NOTIFICATIONS";
    public static class NotificationTask extends AsyncTask<Void, Void, String> {

        private NotificationManager mNotificationManager;

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(Void... voids) {

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(MainActivity.activity, "notify_001");
            Intent ii = new Intent(MainActivity.activity, UI_IntroWelcomeActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.activity, 0, ii, 0);

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            try {
                bigText.setBigContentTitle(MainActivity.notificationData.getJSONObject(0).getString("name"));
                bigText.bigText(MainActivity.notificationData.getJSONObject(0).getString("id")+" x "+MainActivity.notificationData.getJSONObject(0).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bigText.setSummaryText("INFORMATION");
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setSmallIcon(R.drawable.ic_logo_hello_dati);
            mBuilder.setContentTitle("Your Title");
            mBuilder.setContentText("Your text");
            mBuilder.setPriority(Notification.PRIORITY_MAX);
            mBuilder.setStyle(bigText);
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            mNotificationManager =
                    (NotificationManager) MainActivity.activity.getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = "Your_channel_id";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);

            mNotificationManager.notify(0, mBuilder.build());
            return null;
        }
    }

    //----------------- Notification
    //----------------- Update
    public static class UpdateTask extends AsyncTask<String, Integer, Boolean> {

        String TAG = "DownloadUpdate";
        DownloadUpdate.DownloadListener downloadListener;
        final String path = Environment.getExternalStorageDirectory() + "/Download/";
        String apkURL;


        public void tryStartDownload(final String apkUrl, final String appName) {
            new Thread(new Runnable() {
                public void run() {
                    if (DatiUpdateState.getState(MainActivity.activity) != DatiUpdateState.STATE_IDLE) {

                    }
                    Log.i(TAG, "start downloading newest version ");
                    DatiUpdateState.setState(MainActivity.activity.getApplicationContext(), DatiUpdateState.STATE_DOWNLOADING);
                    Boolean flag = false;
                    try {
                        URL url = new URL(apkUrl);
                        HttpURLConnection c = (HttpURLConnection) url.openConnection();
                        c.setRequestMethod("GET");
                        c.setDoOutput(true);
                        c.connect();
                        FileOutputStream fos;
                        File file = new File(path);
                        file.mkdirs();
                        File outputFile = new File(file, appName);
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }
                        fos = new FileOutputStream(outputFile);
                        InputStream is = c.getInputStream();
                        int total_size = 1431692;//size of apk
                        byte[] buffer1 = new byte[1024];
                        int len1 = 0;
                        int per = 0;
                        int downloaded = 0;
                        while ((len1 = is.read(buffer1)) != -1) {
                            fos.write(buffer1, 0, len1);
                            downloaded += len1;
                            per = (int) (downloaded * 100 / total_size);
                            publishProgress(per);
                        }
                        fos.flush();
                        fos.close();
                        is.close();
                        DatiUpdateState.setState(MainActivity.activity.getApplicationContext(), DatiUpdateState.STATE_IDLE);
                        Log.i(TAG, "download success");
                        Log.e(TAG, "Installing start ...");
                        DatiUpdateState.setState(MainActivity.activity.getApplicationContext(), DatiUpdateState.STATE_INSTALLING);
                        InputStream in = new FileInputStream(path + appName);
                        PackageInstaller packageInstaller = MainActivity.activity.getPackageManager().getPackageInstaller();
                        PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
                        params.setAppPackageName("com.android.hellodatiproject");
                        // set params
                        int sessionId = packageInstaller.createSession(params);
                        PackageInstaller.Session session = packageInstaller.openSession(sessionId);
                        OutputStream out = session.openWrite("com.android.hellodatiproject", 0, -1);
                        byte[] buffer2 = new byte[65536];
                        int c2;
                        while ((c2 = in.read(buffer2)) != -1) {
                            out.write(buffer2, 0, c2);
                        }
                        session.fsync(out);
                        in.close();
                        out.close();
                        session.commit(PendingIntent.getBroadcast(MainActivity.activity.getApplicationContext(), sessionId, new Intent("android.intent.action.MAIN"), 0).getIntentSender());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        public void setParams(String apkURL) {
            this.apkURL=apkURL;
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            tryStartDownload(
                    apkURL,
                    URLUtil.guessFileName(apkURL, null, null));
            return null;
        }
    }

    //----------------- Update
}
