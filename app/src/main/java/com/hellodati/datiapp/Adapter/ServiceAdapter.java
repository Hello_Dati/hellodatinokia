package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.UI_ContactFragment;
import com.hellodati.datiapp.UI.UI_DrinksFragment;
import com.hellodati.datiapp.UI.UI_EventFragment;
import com.hellodati.datiapp.UI.UI_LeisureFragment;
import com.hellodati.datiapp.UI.UI_MeetingFragment;
import com.hellodati.datiapp.UI.UI_RestaurantFragment;
import com.hellodati.datiapp.UI.UI_ServiceFragment;
import com.hellodati.datiapp.UI.UI_WellBeingFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder>{

    JSONArray data;
    Context context;

    public ServiceAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_service, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.title.setText(data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (i){
                    //restaurant
                    case 0:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_RestaurantFragment());
                        break;
                    //drinks
                    case 1:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_DrinksFragment());
                        break;
                    //leisure
                    case 2:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_LeisureFragment());
                        break;
                    //well-being
                    case 3:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_WellBeingFragment());
                        break;
                    //events
                    case 4:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_EventFragment());
                        break;
                    //meeting
                    case 5:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_MeetingFragment());
                        break;
                    //concierge
                   /* case 6:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_RestaurantFragment());
                        break;*/
                    //contacts
                    case 6:
                        UI_ServiceFragment.selectedService=viewHolder.title.getText().toString();
                        setFragmentOfTab(new UI_ContactFragment());
                        break;
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    // set the data in the content place
    public static void setFragmentOfTab(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = ((AppCompatActivity) UI_ServiceFragment.context).getSupportFragmentManager().beginTransaction();
            ft.replace(UI_ServiceFragment.frameLayout.getId(), fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image_view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
