package com.hellodati.datiapp.Implementation;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hellodati.datiapp.UI.UI_EventFragment;
import com.hellodati.datiapp.UI.UI_ServiceFragment;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;

public class iEvent {
    public static void getEventData(final JSONArray data) throws JSONException {
        UI_EventFragment.position=0;
        if(data==null){
            UI_ServiceFragment.frameLayout.removeAllViews();
        }
        else {
            UI_EventFragment.carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
                    Glide.with(UI_EventFragment.context).load("https://image.freepik.com/free-vector/music-event-banner-template-with-photo_52683-12627.jpg").into(imageView);
                }
            });
            UI_EventFragment.carouselView.setPageCount(data.length());
            UI_EventFragment.carouselView.setImageClickListener(new ImageClickListener() {
                @Override
                public void onClick(int position) {
                    UI_EventFragment.position=1;
                    UI_EventFragment.tabs.setVisibility(View.VISIBLE);
                    UI_EventFragment.buttomHeader.setVisibility(View.VISIBLE);
                    UI_EventFragment.carouselLayout.setVisibility(View.GONE);
                    try {
                        UI_EventFragment.eventTitle.setText(data.getJSONObject(position).getString("name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public static void getEventDataError(int errorCode, String errorMSG){

    }
}
