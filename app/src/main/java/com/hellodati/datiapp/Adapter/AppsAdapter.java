package com.hellodati.datiapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.AppInfo;

import java.util.ArrayList;
import java.util.Objects;

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder>{
    ArrayList<AppInfo> data;
    Context context;



    public AppsAdapter(ArrayList<AppInfo> data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_app, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.appName.setText(data.get(i).label);
        viewHolder.appIcon.setImageDrawable(data.get(i).icon);
        viewHolder.appItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchIntent = Objects.requireNonNull(context).getPackageManager().getLaunchIntentForPackage(data.get(i).packageName);
                context.startActivity(launchIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout appItem;
        TextView appName;
        ImageView appIcon;

        public ViewHolder(final View itemView) {
            super(itemView);
            appItem=itemView.findViewById(R.id.app_item);
            appName=itemView.findViewById(R.id.app_name);
            appIcon=itemView.findViewById(R.id.app_icon);
        }
    }
}
