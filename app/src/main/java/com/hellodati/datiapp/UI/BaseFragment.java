package com.hellodati.datiapp.UI;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public abstract void onFragmentLoad();

    public abstract void onFragmentUnload();

    public void onFragmentReload() {
        onFragmentUnload();
        onFragmentLoad();
    }
}
