package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.hellodati.datiapp.Adapter.BasketAdapter;
import com.hellodati.datiapp.UI.UI_BasketFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iBasketFragment {

    public static void getBasketData(final JSONArray data) throws JSONException {
        UI_BasketFragment.recyclerViewBasket.setLayoutManager(new LinearLayoutManager(UI_BasketFragment.context));
        BasketAdapter adapter = new BasketAdapter(data, UI_BasketFragment.context);
        UI_BasketFragment.recyclerViewBasket.setAdapter(adapter);
        UI_BasketFragment.progressBar.setVisibility(View.GONE);
    }


    public static void getBasketDataError(int errorCode, String errorMSG){
        Log.e("ServiceFragment : get hotel data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

    public static void getDeliveryPlacesData(final JSONArray data) throws JSONException {

    }


    public static void getDeliveryPlacesError(int errorCode, String errorMSG){
        Log.e("ServiceFragment : get delivery places data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

}
