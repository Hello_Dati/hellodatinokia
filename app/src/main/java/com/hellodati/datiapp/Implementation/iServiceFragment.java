package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.hellodati.datiapp.Adapter.ServiceAdapter;
import com.hellodati.datiapp.UI.UI_ServiceFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iServiceFragment {

    public static void getHotelData(JSONArray data) throws JSONException {
        UI_ServiceFragment.hotelTitle.setText(data.getJSONObject(0).getString("name"));
        UI_ServiceFragment.hotelStars.setRating(Float.parseFloat(data.getJSONObject(0).getString("id")));
    }

    public static void getHotelDataError(int errorCode, String errorMSG){
        Log.e("ServiceFragment : get hotel data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

    public static void getServiceData(JSONArray data) throws JSONException {
        UI_ServiceFragment.recyclerView.setLayoutManager(new LinearLayoutManager(UI_ServiceFragment.context));
        ServiceAdapter adapter = new ServiceAdapter(data, UI_ServiceFragment.context);
        UI_ServiceFragment.recyclerView.setAdapter(adapter);
    }

    public static void getServiceDataError(int errorCode, String errorMSG){

    }

}
