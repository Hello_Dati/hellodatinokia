package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;

import com.hellodati.datiapp.Adapter.MeetAdapter;
import com.hellodati.datiapp.UI.UI_MeetingListFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iMeetAdapter {

    public static void getMeetListData(JSONArray data) throws JSONException {
        UI_MeetingListFragment.meetRecyclerView.setLayoutManager(new LinearLayoutManager(UI_MeetingListFragment.context));
        MeetAdapter adapter = new MeetAdapter(data, UI_MeetingListFragment.context);
        UI_MeetingListFragment.meetRecyclerView.setAdapter(adapter);
    }

    public static void getMeetListDataError(int errorCode, String errorMSG){

    }

}
