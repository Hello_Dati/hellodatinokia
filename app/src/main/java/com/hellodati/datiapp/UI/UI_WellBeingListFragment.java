package com.hellodati.datiapp.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hellodati.datiapp.DAO.State;
import com.hellodati.datiapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_WellBeingListFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView recyclerWellBeingActivites,recyclerWellBeingOffers;

    public static State stateDAO=new State();

    public UI_WellBeingListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        UI_WellBeingFragment.position=0;
        UI_WellBeingFragment.tabs.setVisibility(View.GONE);
        UI_WellBeingFragment.buttomHeader.setVisibility(View.GONE);
        stateDAO.getAll("fr","iWellBeingAdapter.getWellBeingActivitiesListData","iWellBeingAdapter.getWellBeingActivitiesListDataError");
        stateDAO.getAll("fr","iWellBeingAdapter.getWellBeingOffersListData","iWellBeingAdapter.getWellBeingOffersListDataError");
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui__well_being_list, container, false);
        recyclerWellBeingActivites=view.findViewById(R.id.recycler_well_being_activites);
        recyclerWellBeingOffers=view.findViewById(R.id.recycler_well_being_offers);
    }

}
