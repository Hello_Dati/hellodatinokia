package com.hellodati.datiapp.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.hellodati.datiapp.Adapter.ReviewsAdapter;
import com.hellodati.datiapp.UI.UI_ReviewsFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iReviewsAdapter {
    public static void getReviewsData(JSONArray data) throws JSONException {
        //data=null;
        if(data==null){
            UI_ReviewsFragment.emptyBasket.setVisibility(View.VISIBLE);
            UI_ReviewsFragment.scrollReview.setVisibility(View.GONE);
        }
        else {
            UI_ReviewsFragment.emptyBasket.setVisibility(View.GONE);
            UI_ReviewsFragment.scrollReview.setVisibility(View.VISIBLE);
            UI_ReviewsFragment.recyclerReviews.setLayoutManager(new LinearLayoutManager(UI_ReviewsFragment.context));
            ReviewsAdapter adapter = new ReviewsAdapter(data, UI_ReviewsFragment.context);
            UI_ReviewsFragment.recyclerReviews.setAdapter(adapter);
        }
    }

    public static void getReviewsDataError(int errorCode, String errorMSG){

    }
}
