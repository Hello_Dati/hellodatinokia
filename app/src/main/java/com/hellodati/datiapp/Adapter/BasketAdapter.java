package com.hellodati.datiapp.Adapter;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellodati.datiapp.R;
import com.hellodati.datiapp.UI.UI_BasketFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder>{
    JSONArray data;
    Context context;



    public BasketAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_basket, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final int[] qtCommande = {1};
        final double unitPrice =35.5;

        try {

            if (i%2 ==0){
                viewHolder.relativeLayout.setBackgroundResource(R.color.white);
            }else {
                viewHolder.relativeLayout.setBackgroundResource(R.color.offwhite);
            }
            viewHolder.commandeName.setText(data.getJSONObject(i).getString("name"));

            viewHolder.dateOfCommande.setText(data.getJSONObject(i).getString("name"));


            viewHolder.totalPrice.setText(String.format("%s Dt", unitPrice));
            viewHolder.quantityVar.setText(String.valueOf(qtCommande[0]));
            viewHolder.incrementQt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   qtCommande[0]++;
                    viewHolder.quantityVar.setText(String.valueOf(qtCommande[0]));
                    viewHolder.totalPrice.setText(String.format("%s Dt", unitPrice * qtCommande[0]));
                }
            });
            viewHolder.decrimentQt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (qtCommande[0] >1){
                        qtCommande[0]--;
                        viewHolder.quantityVar.setText(String.valueOf(qtCommande[0]));
                        viewHolder.totalPrice.setText(String.format("%s Dt", unitPrice * qtCommande[0]));
                    }
                }
            });
            viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteItem();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void deleteItem(){
        final Dialog myDialog = new Dialog(UI_BasketFragment.context);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View confirmView = LayoutInflater.from(UI_BasketFragment.context).inflate(R.layout.popup_delete_item, null, false);
        myDialog.setContentView(confirmView);
        confirmView.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        confirmView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete & toast & reload
                /*HelloDatiApplication.get().getHelloDatiService().deleteShoppingCartOrder(shoppingCartOrder.getId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new MyDisposableObserver<ShoppingCartResponse>() {
                            @Override
                            public void onNext(ShoppingCartResponse shoppingCartResponse) {
                                Toast.makeText(activity, activity.getResources().getString(R.string.shopping_item_removed), Toast.LENGTH_SHORT).show();
                                if (activity instanceof MainActivity) {
                                    ((MainActivity) activity).updateShoppingNotifNum();
                                }
                                ShoppingCartFragment.loadTab(bindingView.tabs.getSelectedTabPosition());
                            }

                            @Override
                            public void onComplete() {
                            }

                            @Override
                            public void onUnhandledError(Throwable e) {
                                if (e instanceof com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                                    com.jakewharton.retrofit2.adapter.rxjava2.HttpException error = (com.jakewharton.retrofit2.adapter.rxjava2.HttpException) e;

                                    if (error.code() == 200) {
                                        Toast.makeText(activity, activity.getResources().getString(R.string.shopping_item_removed), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(activity, activity.getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(activity, activity.getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                                }
                                if (activity instanceof MainActivity) {
                                    ((MainActivity) activity).updateShoppingNotifNum();
                                }
                                ShoppingCartFragment.loadTab(((TabLayout) bindingView.tabs).getSelectedTabPosition());
                            }
                        });*/
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView commandeName,quantityVar,dateOfCommande,totalPrice;
        ImageView incrementQt,decrimentQt,trash;
        RelativeLayout relativeLayout;

        public ViewHolder(final View itemView) {
            super(itemView);
            commandeName = itemView.findViewById(R.id.title);
            quantityVar = itemView.findViewById(R.id.qt_var);
            dateOfCommande = itemView.findViewById(R.id.date);
            totalPrice = itemView.findViewById(R.id.price);
            incrementQt = itemView.findViewById(R.id.inc_qt);
            decrimentQt = itemView.findViewById(R.id.dec_qt);
            trash = itemView.findViewById(R.id.delete_btn);
            relativeLayout = itemView.findViewById(R.id.item_purchase);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
